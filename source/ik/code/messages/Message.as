package ik.code.messages
{
	
	public class Message
	{
		public static const CLICKED:String = "msgClicked";
		public static const DOUBLE_CLICKED:String = "msgDClicked";
		public static const PRESSED:String = "msgPressed";
		public static const COMPLETE:String = "msgComplete";
		public static const UPDATED:String = "msgUpdated";
		public static const OPEN:String = "msgOpen";
		public static const CLOSE:String = "msgClose";
		public static const START:String = "msgStart";
		public static const QUIT:String = "msgQuit";
		public static const SELECT:String = "msgSelect";
		public static const STOPPED:String = "msgStopped";
		public static const CHANGE:String = "msgChange";
	}
	
}